<?php
/**
 * GoogleAnalitics 
 *
<example>
        <?php $this->widget('ext.GoogleAnalitics.GoogleAnalitics', array(
                'siteName'   => 'yoursite.com',
                'trackingId' => '',
        )); ?>
</example>

 * 
 * @uses CWidget
 * @package 
 * @version 1.0
 * @copyright 
 * @author vi mark <webvimark@gmail.com> 
 * @license MIT
 */
class GoogleAnalitics extends CWidget
{
        /**
         * siteName 
         *
         * For example: 'yoursite.com'
         * without 'http://'
         * 
         * @var string
         */
        public $siteName;

        /**
         * trackingId 
         * 
         * @var string
         */
        public $trackingId;


        public function init()
        {
                // Show script only on choosen site and if trackingId is set
                if ( ! $this->trackingId OR ($_SERVER['HTTP_HOST'] != $this->siteName) )
                        return;


                echo <<<JS
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                  ga('create', '$this->trackingId', '$this->siteName');
                  ga('send', 'pageview');

                </script>
JS;
        }
}
